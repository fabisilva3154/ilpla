<?php 
	
	require_once("twigController.php");

	session_start();

	if(isset($_SESSION["admin"])) {
		echo $twig->render('admin/admin.html', array());	
	}
	else {
		header("location:login");
	}

?>