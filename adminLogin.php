<?php

	require_once('twigController.php');
	 
	session_start();

	if(isset($_SESSION["admin"])) 
		header("location:admin.php");
	
	$template = $twig->loadTemplate("login.html");
	$template->display(array('error'=>isset($_GET['error'])));
	
?>