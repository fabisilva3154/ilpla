<?php 
	require_once('model/integranteModel.php');
	include('clases/elementoAutocompletar.php');


	$investigadores = obtenerIdNombreInvestigador($_GET['term']);
	$arregloInvestigadores = array();
	foreach ($investigadores[1] as $investigador) {
		array_push($arregloInvestigadores, new ElementoAutocompletar($investigador[1], $investigador[0]));
	}
	echo json_encode($arregloInvestigadores);	

?>