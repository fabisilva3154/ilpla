<?php 
	
	require_once('model/integranteModel.php');

	session_start();

	if(isset($_SESSION["admin"])) {
		borrarInvestigador($_GET['idInvestigador']);
		header('location:integrantes.php');
	}
	else {
		header("location:login");
	}

?>