<?php 
	
	require_once('model/proyectoModel.php');

	session_start();

	if(isset($_SESSION["admin"])) {
		borrarProyecto($_GET['idProyecto']);
		header('location:proyectos.php');
	}
	else {
		header("location:login");
	}

?>