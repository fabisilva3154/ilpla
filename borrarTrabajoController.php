<?php 
	
	require_once('model/trabajoModel.php');

	session_start();

	if(isset($_SESSION["admin"])) {
		borrarTrabajo($_GET['idTrabajo']);
		header('location:trabajos.php');
	}
	else {
		header("location:login");
	}

?>