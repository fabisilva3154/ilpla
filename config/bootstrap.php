<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$isDevMode = true;
$config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/yaml"), $isDevMode);

// database configuration parameters
$conn = array(
	'driver'   => 'pdo_mysql',
	'dbname'   => 'ilpla',
    'user'     => 'root',
    'password' => 'esqueleto',
    'charset'  => 'utf8',
);

// obtaining the entity manager
global $entityManager; 
$entityManager = EntityManager::create($conn, $config);

?>
