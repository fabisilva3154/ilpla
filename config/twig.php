<?php
	
	global $twig;

	$loader = new Twig_Loader_Filesystem('templates');
	$twig   = new Twig_Environment($loader, array());

?>