<?php

class Users {

    public $entityManager;

    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    public function login($password, $username = NULL)
    {
        if ( !$username ){ $username = 'admin'; }
        $user = $this->getUser(array( 'username' => $username, 'password' => $password ));
        if ( $user ){
            $_SESSION['admin'] = true;
            $_SESSION['user_id'] = $user->getId();
            return true;  
        }
        return false;
    }
    
    public function logout()
    {
        unset($_SESSION['admin']);
        unset($_SESSION['user_id']);
        session_destroy();
    }

    public function isLogged()
    {
        return isset($_SESSION['admin']) ? $_SESSION['admin'] : false;
    }

    public function getUser($values)
    {
        global $entityManager;
        $userRepository = $entityManager->getRepository('User');
        return $userRepository->findOneBy($values);
    }

    public function changePassword($password)
    {
        global $entityManager;
        $userRepository = $entityManager->getRepository('User');
        $user = $userRepository->find($_SESSION['user_id']);
        $user->setPassword($password);
        $user->setReactivate(uniqid());
        $entityManager->persist($user);
        $entityManager->flush();
        // $user->sendMailReactivate();
    }

}

?>