<?php
	

	require_once("vendor/autoload.php");
	require_once("config/twig.php");
	require_once('model/proyectoModel.php');

	session_start();

	if(isset($_SESSION["admin"])) {
		extract($_POST);
		guardarProyecto($idInvestigador, $year, $name);
	 	header('location:proyectos.php');
	}
	else {
		header("location:login");
	}
		
?>