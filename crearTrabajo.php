<?php 
	
	require_once("vendor/autoload.php");
	require_once("config/twig.php");

	session_start();

	if ( isset($_SESSION["admin"]) ) {
		echo $twig->render('admin/crearTrabajo.html', array('logged'=>true));
	} else {
		header("location:login");
	}

	

?>