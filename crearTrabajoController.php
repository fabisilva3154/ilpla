<?php

	require_once("vendor/autoload.php");
	require_once("config/twig.php");
	require_once('model/trabajoModel.php');

	function inputs($elemento, $clave) {
	    echo $elemento;
	}

	session_start();

	if(isset($_SESSION["admin"])) {
		extract($_POST);
		$tags = explode(',', $_POST['idInv']);
		guardarTrabajo($name, $year, $doi, $tags);
	 	header('location:trabajos.php');
	}
	else {
		header("location:login");
	}
		
?>