<?php 

	require_once("vendor/autoload.php");
	require_once("config/twig.php");
	require_once('model/integranteModel.php');

	session_start();

	if ( isset($_SESSION["admin"]) ) {
		$investigadores = obtenerInvestigador();
		echo json_encode($investigadores[1]);
	}

?>