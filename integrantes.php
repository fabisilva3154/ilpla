<?php 

	require_once("vendor/autoload.php");
	require_once("config/twig.php");
	require_once('model/integranteModel.php');

	session_start();

	if(isset($_SESSION["admin"])) {
		$investigadores = obtenerInvestigador();

		$template = $twig->loadTemplate("admin/integrantes.html");
		$template->display(array('investigadores'=>$investigadores[1], 'logged'=>true));
	}
	else {
		header("location:login");
	}

?>