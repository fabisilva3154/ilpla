$(document).ready(function(){

  $('.flexslider').flexslider({
    animation: "fade",
    start: function(slider){
      $('body').removeClass('loading');
    }
  });

  $('.menu-toggle').click(function(e) {
    e.preventDefault();
    $(".menu").fadeToggle(100);
  });

  $(".menu-mobile").click( function (){
    $(".navbar-mobile").fadeToggle(450);
  });  

  $(function () {
    $('a[href*=#]:not([href=#])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: (target.offset().top)
                }, 850);
                return false;
            }
        }
    });
});

  $(document).ready(function(){
  $('.bxslider').bxSlider({
    controls: false,
  });
});

});



