function borrado(id) {
    if (confirm("¿Desea borrar este investigador?")) 
        location.href = "borrarInvestigadorController.php?idInvestigador=" + id;
}

function borradoProyecto(id) {
    if (confirm("¿Desea borrar este proyecto?")) 
        location.href = "borrarProyectoController.php?idProyecto=" + id;
}

function borradoTrabajo(id) {
    if (confirm("¿Desea borrar este trabajo?")) 
        location.href = "borrarTrabajoController.php?idTrabajo=" + id;
}
function getInvestigadores(options){
    var options = $.extend({
        type : 'GET',
        url  : 'getIntegrantes.php',
        dataType : 'json',
    }, options);
    $.ajax(options);
}
function readIMG(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#exampleIMG').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function obtenerUltimoTagYSetearId(id) {
    var arreglo = $("input[name='tags[]']");
    var input = arreglo[arreglo.length-1];
    $(input).attr("value", valorIdItem);
}

function split( val ) {
  return val.split( /,\s*/ );
}

function extractLast( term ) {
  return split( term ).pop();
}

$(document).ready(function(){

    // $("#nombreInvestigador").autocomplete({
    //     source: "autocompletarInvestigador.php",
      
    //     change: function (event, ui) {
    //         $("#nombreInvestigador").val(ui.item.label);
    //         $("#idInvestigador").val(ui.item.value);
    //     },

    //     focus: function(event, ui) {
    //         event.preventDefault();
    //         $("#nombreInvestigador").val(ui.item.label);
    //     }
    // })

    //CODIGO PARA AGREGAR IDS A CADA INVESTIGADOR (TAG), EN MODIFICAR TRABAJO
    if ($('#idInv').length) {
        var data = $('#idInv').val();
        var arregloIds = data.split(',');
        $("input[name='tags[]']").each(function(index) {
            this.value = arregloIds[index];
        })
    }

    //Vista previa de imagen en integrante
    if ($('#archivo').length) {
        $('#archivo').change(function(){
            readIMG(this);
        })
    }

    //option selected para modificacion de integrante
    if ($('#selectCategoriaAux').length) {
        var valorSelect = $('#selectCategoriaAux').val();
        $('#selectCategoria').val(valorSelect).attr('selected', 'selected');
    }

});