<?php

	require_once('errorModel.php');

	function obtenerInvestigador(){
		include('conexion.php');

		$items = $db->query("SELECT * FROM investigador");
		$db = null;

		verificarError($items);
		return array($items->rowCount(), $items->fetchAll());
	}

	function obtenerIdNombreInvestigador($autocomplete){
		include('conexion.php');

		$stmt = $db->prepare("SELECT idInvestigador, nombre FROM investigador WHERE nombre LIKE CONCAT('%',:nombre,'%')");
		// $datos = array($autocomplete);
		$stmt->bindValue('nombre', $autocomplete);
		$stmt->execute();
		return array($stmt->rowCount(), $stmt->fetchAll());
	}

	function obtenerInvestigadorConId($id){
		include('conexion.php');

		$items = $db->prepare("SELECT * FROM investigador WHERE idInvestigador = :id");
		$items->bindValue('id', $id);
		$items->execute();
		$db = null;
		return $items->fetch();
	}
	function obtenerInvestigadorPorCategorias(){
		include('conexion.php');

		$categorias = array('Administración', 'Investigador', 'Personal de apoyo', 'Becario', 'Pasante');
		$porCategorias = array();
		foreach ($categorias as $categoria) {
			$stmt = $db->prepare("SELECT * FROM investigador WHERE categoria = :categoria");
			$stmt->bindValue('categoria', $categoria);
			$stmt->execute();
			$porCategorias[$categoria] = $stmt->fetchAll();
		}
		$db = null;
		return $porCategorias;	
	}


	function guardarInvestigador($nombre, $titulo, $areaInvestigacion, $dependencia, $actividad, $email, $telefono, $imagenInvestigador, $categoria){
		include("conexion.php");

		$guardarInvestigador = 'INSERT INTO investigador (idInvestigador, titulo, areaInvestigacion, dependencia, actividad, categoria, email, telefono, nombre, rutaImagen) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
		$consulta = $db->prepare($guardarInvestigador);
		$datos = array('NULL', $titulo, $areaInvestigacion, $dependencia, $actividad, $categoria, $email, $telefono, $nombre, $imagenInvestigador);
		$resultado = $consulta->execute($datos);
		$db = null;

		verificarError($resultado);
	}
			
	function actualizarInvestigador($idInvestigador, $nombre, $titulo, $areaInvestigacion, $dependencia, $actividad, $email, $telefono, $imagenInvestigador, $categoria){
		include("conexion.php");

		$actualizarInvestigador = "UPDATE investigador SET nombre = ?, titulo = ?, areaInvestigacion = ?, dependencia = ?, categoria = ?, actividad = ?, email = ?, telefono = ?, rutaImagen = ? WHERE idInvestigador = ?";
		$consulta = $db->prepare($actualizarInvestigador);
		$datos = array($nombre, $titulo, $areaInvestigacion, $dependencia, $categoria, $actividad, $email, $telefono, $imagenInvestigador, $idInvestigador);
		$resultado = $consulta->execute($datos);
		$db = null;

		verificarError($resultado);
	}

	function borrarInvestigador($idInvestigador){
		include("conexion.php");

		$consulta = "DELETE FROM investigador WHERE idInvestigador = ?";
		$sql = $db->prepare($consulta);
		$datos = array($idInvestigador);
		$consulta = $sql->execute($datos);
		$db = null;

		verificarError($consulta);
	}
	
?>