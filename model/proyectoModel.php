<?php

	require_once('errorModel.php');

	function obtenerProyecto(){
		include('conexion.php');

		$items = $db->query("SELECT * FROM proyecto");
		$db = null;

		verificarError($items);

		return array($items->rowCount(), $items->fetchAll());
	}

	function obtenerProyectoConId($id){
		include('conexion.php');

		$items = $db->prepare("SELECT * FROM proyecto WHERE idProyecto = ?");
		$datos = array($id);
		$resultado = $items->execute($datos);
		$db = null;
		return $items->fetch();
	}

	function guardarProyecto($idInvestigador, $ano, $nombreProyecto){
		include("conexion.php");

		$guardarProyecto = 'INSERT INTO proyecto (idProyecto, idInvestigador, ano, nombreProyecto) VALUES (?, ?, ?, ?)';
		$consulta = $db->prepare($guardarProyecto);
		$datos = array('NULL', $idInvestigador, $ano, $nombreProyecto);
		$resultado = $consulta->execute($datos);
		$db = null;

		verificarError($resultado);
	}
			
	function actualizarProyecto($idProyecto, $idInvestigador, $ano, $nombreProyecto){
		include("conexion.php");

		$actualizarProyecto = "UPDATE proyecto SET idInvestigador = ?, ano = ?, nombreProyecto = ? WHERE idProyecto = ?";
		$consulta = $db->prepare($actualizarProyecto);
		$datos = array($idInvestigador, $ano, $nombreProyecto, $idProyecto);
		$resultado = $consulta->execute($datos);
		$db = null;

		verificarError($resultado);
	}

	function borrarProyecto($idProyecto){
		include("conexion.php");

		$consulta = "DELETE FROM proyecto WHERE idProyecto = ?";
		$sql = $db->prepare($consulta);
		$datos = array($idProyecto);
		$consulta = $sql->execute($datos);
		$db = null;

		verificarError($consulta);
	}

	function obtenerProyectosDeInvestigador($id){
		include('conexion.php');

		$items = $db->prepare("SELECT * FROM proyectos WHERE idInvestigador = ?");
		$datos = array($id);
		$resultado = $consulta->execute($datos);
		$db = null;

		verificarError($items);

		return array($items->rowCount(), $resultado->fetchAll());
	}
	
?>