<?php

	require_once('errorModel.php');

	function obtenerTrabajo(){
		include('conexion.php');

		$items = $db->query("SELECT * FROM trabajopublicado");
		$db = null;

		verificarError($items);

		return array($items->rowCount(), $items->fetchAll());
	}

	function obtenerTrabajoConId($id){
		include('conexion.php');

		$consulta = $db->prepare("SELECT * FROM trabajopublicado WHERE idTrabajo = ?");
		$datos = array($id);
		$consulta->execute($datos);

		$resultadoInvestigadores = obtenerInvestigadores($id);

		$db = null;
		return array($consulta->fetch(), $resultadoInvestigadores);
	}

	function obtenerInvestigadores($id){
		include('conexion.php');

		$consulta = $db->prepare("SELECT it.idInvestigador, inv.nombre FROM investigador_trabajo as it INNER JOIN investigador as inv on it.idInvestigador = inv.idInvestigador WHERE it.idTrabajo = ?");
		$datos = array($id);
		$resultadoInvestigadores = $consulta->execute($datos);

		$db = null;
		return $consulta->fetchAll();
	}

	function guardarTrabajo($nombre, $ano, $doi, $idInvestigador){
		include("conexion.php");
		$db->beginTransaction();
		$guardarTrabajo = 'INSERT INTO trabajopublicado (idTrabajo, ano, nombreTrabajo, DOI) VALUES (?, ?, ?, ?)';
		$consulta = $db->prepare($guardarTrabajo);
		$datos = array('NULL', $ano, $nombre, $doi);
		$resultado = $consulta->execute($datos);

		$idTrabajo = $db->lastInsertId();

		foreach ($idInvestigador as $id) {
			guardarInvestigadorTrabajo($id, $idTrabajo);
		}
		$db->commit();
		$db = null;
	}
			
	function actualizarTrabajo($idTrabajo, $nombre, $ano, $doi, $ids){ //ids va a venir como un arreglo
		include("conexion.php");

		$actualizarTrabajo = "UPDATE trabajopublicado SET nombreTrabajo = ?, ano = ?, DOI = ? WHERE idTrabajo = ?";
		$consulta = $db->prepare($actualizarTrabajo);
		$datos = array($nombre, $ano, $doi, $idTrabajo);
		$resultado = $consulta->execute($datos);

		borrarInvestigadorTrabajo($idTrabajo);
		foreach ($ids as $id) {
			guardarInvestigadorTrabajo($id, $idTrabajo);
		}		

		$db = null;

		verificarError($resultado);
	}

	function guardarInvestigadorTrabajo($idInvestigador, $idTrabajo){
		include("conexion.php");

		$guardarInTr = 'INSERT INTO investigador_trabajo (idInvestigador, idTrabajo) VALUES (:id, :trabajo)';
		$consulta = $db->prepare($guardarInTr);
		$consulta->bindValue('id', $idInvestigador);
		$consulta->bindValue('trabajo', $idTrabajo);
		$consulta->execute();
	}

	function borrarTrabajo($idTrabajo){ 
		include("conexion.php");

		$consulta = "DELETE FROM trabajopublicado WHERE idTrabajo = ?";
		$sql = $db->prepare($consulta);
		$datos = array($idTrabajo);
		$consulta = $sql->execute($datos);

		borrarInvestigadorTrabajo($idTrabajo);

		$db = null;

		verificarError($consulta);
	}

	function borrarInvestigadorTrabajo($idTrabajo){ 
		include("conexion.php");

		$consulta = "DELETE FROM investigador_trabajo WHERE idTrabajo = ?";
		$sql = $db->prepare($consulta);
		$datos = array($idTrabajo);
		$consulta = $sql->execute($datos);

		$db = null;

		verificarError($consulta);
	}	

	function obtenerTrabajosDeInvestigador($id){
		include('conexion.php');

		$items = $db->prepare("SELECT * FROM trabajopublicado as tp INNER JOIN investigador_trabajo as it on tp.idTrabajo = it.idTrabajo WHERE it.idInvestigador = ?");
		$datos = array($id);
		$resultado = $consulta->execute($datos);
		$db = null;

		verificarError($items);

		return array($items->rowCount(), $resultado->fetchAll());
	}
	
?>