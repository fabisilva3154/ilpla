<?php

	require_once("vendor/autoload.php");
	require_once("config/twig.php");
	require_once('model/integranteModel.php');
	
	session_start();

	if(isset($_SESSION["admin"])) {
		extract($_GET);
		$resultado = obtenerInvestigadorConId($idInvestigador);

		$template = $twig->loadTemplate("admin/modificarInvestigador.html");
		$template->display(array('nombre'=>$resultado[8], 'titulo'=>$resultado[1], 'areaInvestigacion'=>$resultado[2], 'dependencia'=>$resultado[3], 'actividad'=>$resultado[5], 'categoria'=>$resultado[4], 'email'=>$resultado[6], 'telefono'=>$resultado[7], 'idInvestigador'=>$resultado[0], 'rutaImagen'=>$resultado[9], 'logged' => true));
	}
	else {
		header("location:login");
	}
		
?>