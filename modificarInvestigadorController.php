<?php

	require_once("vendor/autoload.php");
	require_once('model/integranteModel.php');

	session_start();

	if(isset($_SESSION["admin"])) {
		extract($_POST);
		if (isset($_FILES['archivo']) && ($_FILES['archivo']['type'] == 'image/jpeg' || $_FILES['archivo']['type'] == 'image/png' || $_FILES['archivo']['type'] == 'image/gif') && $_FILES['archivo']['size'] < 1048576) {
			//$extension = substr($_FILES['archivo']['name'], strlen($_FILES['archivo']['name'])-4, strlen($_FILES['archivo']['name']));
			if (move_uploaded_file($_FILES['archivo']['tmp_name'], 'imagenes/integrantes/'.$_FILES['archivo']['name'])) {
				if ($rutaArchivo != 'imagenes/integrantes/'.$_FILES['archivo']['name']) {
					//borrar archivo viejo
					unlink($rutaArchivo);
				}
				$rutaArchivo = 'imagenes/integrantes/'.$_FILES['archivo']['name'];
			}
			else {
				die("No se puede subir la imagen al servidor. Pruebe nuevamente en unos minutos. Disculpe las molestias ocasionadas.");
			}
		}

		actualizarInvestigador($idInvestigador, $name, $degree, $researchArea, $dependence, $activity, $email, $phone, $rutaArchivo, $selectCategoria);
	 	header('location:integrantes.php');
	}
	else {
		header("location:login");
	}

?>