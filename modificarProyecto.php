<?php

	require_once("vendor/autoload.php");
	require_once('model/proyectoModel.php');
	require_once('model/integranteModel.php');
	require_once("config/twig.php");

	session_start();

	if(isset($_SESSION["admin"])) {
		extract($_GET);
		$resultado = obtenerProyectoConId($idProyecto);
		$investigador = obtenerInvestigadorConId($resultado['idinvestigador']);

		$template = $twig->loadTemplate("admin/modificarProyecto.html");
		$template->display(array('nombreProyecto'=>$resultado[3], 'ano'=>$resultado[2],'nombreInvestigador'=>$investigador['nombre'] ,'idInvestigador'=>$resultado[1], 'idProyecto'=>$resultado[0], 'logged' => true));
	}
	else {
		header("location:login");
	}
		
?>