<?php

	require_once('model/proyectoModel.php');

	session_start();

	if(isset($_SESSION["admin"])) {
		extract($_POST);
		actualizarProyecto($idProyecto, $idInvestigador, $year, $name);
	 	header('location:proyectos.php');
	}
	else {
		header("location:login");
	}
		
?>