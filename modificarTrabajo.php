<?php

	require_once("vendor/autoload.php");
	require_once("model/trabajoModel.php");
	require_once("config/twig.php");


	session_start();

	if(isset($_SESSION["admin"])) {
		extract($_GET);
		$resultadoTp = obtenerTrabajoConId($idTrabajo);

		$ids = "";
		foreach ($resultadoTp[1] as $tupla) {
			$ids .= $tupla[0] . ",";
		}

		$ids = rtrim ($ids,",");

		$template = $twig->loadTemplate("admin/modificarTrabajo.html");
		$template->display(array('idTrabajo'=>$resultadoTp[0][0], 'ano'=>$resultadoTp[0][1], 'nombreTrabajo'=>$resultadoTp[0][2], 'doi'=>$resultadoTp[0][3], 'investigadores'=>$resultadoTp[1], 'idInv'=>$ids, 'logged' => true));
	} else {
		header("location:login");
	}
		
?>