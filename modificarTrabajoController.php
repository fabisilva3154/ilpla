<?php

	require_once('model/trabajoModel.php');

	session_start();

	if(isset($_SESSION["admin"])) {
		extract($_POST);
		$tags = explode(',', $_POST['idInv']);
		actualizarTrabajo($idTrabajo, $name, $year, $doi, $tags);
	 	header('location:trabajos.php');
	}
	else {
		header("location:login");
	}
		
?>