<?php 

	require_once("twigController.php");
	require_once('/model/integranteModel.php');
	require_once('/model/trabajoModel.php');

	if (isset($_GET['idInvestigador'])) {
		$investigador = obtenerInvestigadorConId($_GET['idInvestigador']);
		$trabajos = obtenerTrabajosDeInvestigador($_GET['idInvestigador']);

		$template = $twig->loadTemplate("verIntegrante.html");
		if ($trabajos[1] > 0)
			$template->display(array('investigadores'=>$investigador), 'trabajos'=>$trabajos[1]);
		else 
			$template->display(array('investigadores'=>$investigador));
	}

?>