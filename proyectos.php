<?php 
	
	require_once("vendor/autoload.php");
	require_once("config/twig.php");
	require_once("model/proyectoModel.php");

	session_start();

	if(isset($_SESSION["admin"])) {
		$proyectos = obtenerProyecto();
		if ($proyectos[0] > 0)
			echo $twig->render('admin/proyectos.html', array('proyectos'=>$proyectos[1], 'logged'=>true));
		else
			echo $twig->render('admin/proyectos.html', array('logged'=>true));
	}
	else {
		header("location:login");
	}

?>