<?php
	
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);
	
	global $basePath;
	$basePath = '/ilpla';
	
	require('vendor/autoload.php');
	require('config/bootstrap.php');
	require('config/twig.php');
	require('controllers/base.php');
	require('controllers/Users.php');
	require('model/integranteModel.php');
	session_start();
	$router = new AltoRouter();
	$router->setBasePath($basePath);

	//FRONTEND

	$router->map('GET', '/', function(){
		global $twig;
		echo $twig->render('index.html', array());
	});

	$router->map('GET', '/integrantes', function(){
		global $twig;
		$integrantes = obtenerInvestigadorPorCategorias();
		echo $twig->render('integrantes.html', array(
			'administracion' => $integrantes['Administración'],
			'investigadores' => $integrantes['Investigador'],
			'personal' => $integrantes['Personal de apoyo'],
			'becarios' => $integrantes['Becario'],
			'pasantes' => $integrantes['Pasante'],
		));
	});

	$router->map('GET', '/bacteriologia', function(){
		global $twig;
		echo $twig->render('bacteriologia.html', array());
	});

	$router->map('GET', '/bentos-laboratorio', function(){
		global $twig;
		echo $twig->render('bentos-laboratorio.html', array());
	});

	$router->map('GET', '/bentos', function(){
		global $twig;
		echo $twig->render('bentos.html', array());
	});

	$router->map('GET', '/biblioteca_ilpla', function(){
		global $twig;
		echo $twig->render('biblioteca_ilpla.html', array());
	});

	$router->map('GET', '/ciclos-biogeoquimicos', function(){
		global $twig;
		echo $twig->render('ciclos-biogeoquimicos.html', array());
	});

	$router->map('GET', '/ciclos-laboratorio', function(){
		global $twig;
		echo $twig->render('ciclos-laboratorio.html', array());
	});

	$router->map('GET', '/contacto', function(){
		global $twig;
		echo $twig->render('contacto.html', array());
	});

	$router->map('GET', '/cuenca-del-salado', function(){
		global $twig;
		echo $twig->render('cuenca-del-salado.html', array());
	});

	$router->map('GET', '/entomologia', function(){
		global $twig;
		echo $twig->render('entomologia.html', array());
	});

	$router->map('GET', '/extension', function(){
		global $twig;
		echo $twig->render('extension.html', array());
	});

	$router->map('GET', '/herpetologia', function(){
		global $twig;
		echo $twig->render('herpetologia.html', array());
	});

	$router->map('GET', '/ictiologia', function(){
		global $twig;
		echo $twig->render('ictiologia.html', array());
	});

	$router->map('GET', '/insectos-laboratorio', function(){
		global $twig;
		echo $twig->render('insectos-laboratorio.html', array());
	});

	$router->map('GET', '/insectos', function(){
		global $twig;
		echo $twig->render('insectos.html', array());
	});

	$router->map('GET', '/institucional', function(){
		global $twig;
		echo $twig->render('institucional.html', array());
	});

	$router->map('GET', '/investigaciones', function(){
		global $twig;
		echo $twig->render('investigaciones.html', array());
	});

	$router->map('GET', '/novedades', function(){
		global $twig;
		echo $twig->render('novedades.html', array());
	});

	$router->map('GET', '/peces-laboratorio', function(){
		global $twig;
		echo $twig->render('peces-laboratorio.html', array());
	});

	$router->map('GET', '/peces', function(){
		global $twig;
		echo $twig->render('peces.html', array());
	});

	$router->map('GET', '/plancton-laboratorio', function(){
		global $twig;
		echo $twig->render('plancton-laboratorio.html', array());
	});

	$router->map('GET', '/plancton', function(){
		global $twig;
		echo $twig->render('plancton.html', array());
	});

	$router->map('GET', '/proyectos', function(){
		global $twig;
		echo $twig->render('proyectos.html', array());
	});

	$router->map('GET', '/servicios', function(){
		global $twig;
		echo $twig->render('servicios.html', array());
	});

	$router->map('GET', '/bacteriologia', function(){
		global $twig;
		echo $twig->render('bacteriologia.html', array());
	});

	$router->map('GET', '/publicaciones', function(){
		global $twig;
		echo $twig->render('publicaciones.html', array());
	});

	$router->map('GET', '/descargas', function(){
		global $twig;
		echo $twig->render('descargas.html', array());
	});

	//BACKEND
	//GET /login
	$router->map( 'GET', "/login", function(){
		global $twig;
		echo $twig->render('login.html', array());
	});

	//POST /login
	$router->map( 'POST', "/login", function(){
		if ( !empty($_POST['password']) ){
			global $redirect_admin;
			global $redirect_login;
			$users    = new Users();
			$logged   = $users->login($_POST['password'], $_POST['username']);
			$redirect = $logged ? $redirect_admin : $redirect_login;
			header("location:$redirect");
		}
	});

	//GET /admin
	$router->map( 'GET', "/admin", function(){
		$users = new Users();
		if ( !$users->isLogged() ){ 
			global $redirect_no_auth;
			header("location:$redirect_no_auth"); 
		} else {
			global $twig;
			echo $twig->render('admin.html', array(
				'logged' => true,
			));
		}
	});

	//GET /panel
	$router->map( 'GET', "/admin/panel", function(){
		$users = new Users();
		if ( !$users->isLogged() ){ 
			global $redirect_no_auth;
			header("location:$redirect_no_auth"); 
		} else {
			global $twig;
			echo $twig->render('panel.html', array());
		}
	});

	//POST /panel
	$router->map( 'POST', "/admin/panel", function(){
		global $redirect_login;
		global $redirect_admin;
		$users = new Users();
		if ( $users->isLogged() ){ 
			$users->changePassword($_POST['hash']);
			header("location:$redirect_admin");
		} else {
			header("location:$redirect_login");
		}
	});

	//GET /reactivate
	$router->map( 'GET', "/admin/reactivate/[i:token]", function($token){
		$users = new Users();
		$user = $users->getUser(array( 'reactivate' => $token ));
		if ( !$user ){ 
			global $redirect_no_auth;
			header('location:$redirect_no_auth'); 
		} else {
			global $twig;
			echo $twig->render('reactivate.html', array( 'user'  => $user ));
		}
	});

	//GET /logout
	$router->map( 'GET', "/admin/logout", function(){
		$users = new Users();
		$users->logout();
		global $redirect_logout;
		header("location:$redirect_logout");
		exit;
	});


	//matching...
	$match = $router->match();
	// call closure or throw 404 status
	if( $match && is_callable( $match['target'] ) ) {
		call_user_func_array( $match['target'], $match['params'] ); 
	} else {
		// no route was matched
		header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
		echo 'La ruta no engancha con ninguna en routes.php';
	}
?>
