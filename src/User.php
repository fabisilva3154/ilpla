<?php

class User
{
    protected $id;
    protected $username;
    protected $password;
    protected $reactivate;
    protected $email; 

    public function getId()
    {
        return $this->id;
    }
    
    public function getUsername()
    {
        return $this->username;
    }
    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getPassword()
    {
        return $this->password;
    }
    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getReactivate()
    {
        return $this->reactivate;
    }
    public function setReactivate($reactivate)
    {
        $this->reactivate = $reactivate;
    }
    public function generateReactivate()
    {
        return md5(uniqid($more_entropy=TRUE));
    }

    public function getEmail()
    {
        return $this->email;
    }
    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getHeaders(){
        $headers = "From: webmaster@osioylaborde.com\r\n" .
        "Reply-To: webmaster@osioylaborde.com\r\n".
        "MIME-Version: 1.0\r\n".
        "Content-Type: text/html; charset=ISO-8859-1\r\n";
        return $headers;
    }

    public function sendEmailReactivate()
    {
        $token = $this->generateReactivate();
        $this->setReactivate($token);
        $html = "".
        "<body style='font-family: sans-serif;'>".
            "<h3 style='color: #222; font-weight: bold; font-size: 20px; border-bottom: 1px solid #ddd; padding-bottom: 8px;margin-bottom:20px'>Osio y Laborde</h3>".
            "<br/>".
            "<div style='padding-left: 10px;font-size: 14px; font-weight: bold; border-left: 2px solid #BAE0EB'>".
            "<p>La contrase&ntilde;a de accesso de tu sitio web ha cambiado recientemente. Si realizaste este cambio no es necesario hacer nada m&aacute;s.</p>".
            "<p>Si no cambiaste la contrase&ntilde;a, es posible que el sitio haya sido vulnerado. Para volver a acceder a la cuenta, deber&aacute;s reestablecer la contrase&ntilde;a</p>".
            "</div>".
        "</body>".
        "</html>";
        // mail($this->getEmail(), 'Solicitud de cambio de clave', $html, $this->getHeaders());
    }

    public function sendEmailChanged($newEmail)
    {
        $html = "".
        "<body style='font-family: sans-serif;'>".
            "<h3 style='color: #222; font-weight: bold; font-size: 20px; border-bottom: 1px solid #ddd; padding-bottom: 8px;margin-bottom:20px'>Osio y Laborde</h3>".
            "<br/>".
            "<div style='padding-left: 10px;font-size: 14px; font-weight: bold; border-left: 2px solid #BAE0EB'>".
            "<p>El email de contacto de su sitio web ha cambiado recientemente a $newEmail</p>".
            "</div>".
        "</body>".
        "</html>";
        mail($this->getEmail(), 'Cambio de email', $html, $this->getHeaders());
        mail($newEmail, 'Cambio de email', $html, $this->getHeaders());
    }
}
?>