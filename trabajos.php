<?php 
	
	require_once("vendor/autoload.php");
	require_once("config/twig.php");
	require_once("model/trabajoModel.php");

	session_start();

	if(isset($_SESSION["admin"])) {
		$trabajos = obtenerTrabajo();
		if ($trabajos[0] > 0)
			echo $twig->render('admin/trabajos.html', array('trabajos'=>$trabajos[1], 'logged'=>true));
		else
			echo $twig->render('admin/trabajos.html', array('logged'=>true));
	}
	else {
		header("location:login");
	}

?>